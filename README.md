# goBet

I made this to help keep track of bets between friends. It stores the bets in a .csv and generates a markdown report 
of the data. If you want to use it, `go get` it from here, run it, then use the menu to add/move/list/etm. the bets
you have. 

I literally wrote this in two days, so don't use it for anything serious. 